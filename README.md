# PowerShell Audio Player

![Alt text](powershell_audio_player_screenshot.png)

## Description
This simple PowerShell script allows Windows users to browse their Music directory from the command line. It displays all directories and audio files in a list that the user can navigate through. It uses ffplay to play audio files.

Note: Upon exiting/terminating the program, the current working directory will be the last directory the program was in.

## Usage
Run the script `.\chefaudio.ps1`.
Use W, A, S and D keys to navigate:
* W - Move selection up by one. Loops to the bottom if at top.
* S - Move selection down by one. Loops to the top if at bottom.
* A - Move out to the parent directory. Stops at the Music directory.
* D - Moves into directories or plays currently selected audio file in new ffplay window. Close this new window for the program to return to the directory list.

Note: Holding down any key will keep triggering corresponding input. Because of this, holding down the D key may repeatedly play audio files until it catches up.

## License
This project is licensed under the terms of the [GNU AGPLv3](https://gitlab.com/s_l_group/powershell-audio-player/-/blob/main/LICENSE) License.
