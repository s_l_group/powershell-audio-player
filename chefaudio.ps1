﻿$MAX_DISPLAY_ROWS=15
$HALF_DISPLAY_ROWS=$(($MAX_DISPLAY_ROWS/2))

$musicdir="$($env:USERPROFILE)$("\Music")"
$valid="true"
$selected=0
$selectedfile=""


Set-Location $musicdir

while(1)
{
    if($valid = "true")
    {
        Clear-Host
        $start=$selected - $HALF_DISPLAY_ROWS
        
        if($start -lt 0)
        {
            $start=0
        }

        $end=$start + $MAX_DISPLAY_ROWS
        $i=0
        
        foreach($file in Get-ChildItem -Name)
        {
            if($selected -eq $i)
            {
                $selectedfile=$file
                #Write-Host $selectedfile -NoNewline -BackgroundColor Black
                Write-Host "  " -NoNewline -BackgroundColor Green
            }

            if($i -ge $start)
            {
                Write-Host $file -NoNewline
            }

            if($selected -eq $i)
            {
                Write-Host "  " -NoNewline -BackgroundColor Green
            }

            if($i -ge $start)
            {
                Write-Host " "
            }

            $i+=1

            if($i -gt $end)
            {
                Break
            }
        }
    }

    $keyboardinput=$Host.UI.RawUI.ReadKey()
    $valid="true"

    switch($keyboardinput.Character)
    {
        "a" {
            if($PWD.Path -ne $musicdir)
            {
                $selected=0
                Set-Location ..
            }
            Break
        }
        "d" {
            if (Test-Path -Path ($selectedfile -replace '(\[|\])', '`$1') -PathType Leaf)
            {
                Clear-Host
                Write-Host $selectedfile
                ffplay -showmode 1 -loglevel quiet -loop 0 $selectedfile
            }
            else
            {
              $selected=0
              Set-Location $selectedfile
            }
            Break
        }
        "w" {
            $selected-=1
            $limit=(Get-ChildItem | Measure-Object -line).Lines - 1
            if($selected -lt 0)
            {
                $selected=$limit
            }
            Break
        }
        "s" {
            $selected+=1
            $limit=(Get-ChildItem | Measure-Object -line).Lines - 1
            if($selected -gt $limit)
            {
                $selected=0
            }
            Break
        }
        Default {
            $valid="false"
        }
    }
}